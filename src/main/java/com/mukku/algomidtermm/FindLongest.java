/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.algomidtermm;

// cmd; java com\mukku\algomidtermm\FindLongest.java < com\mukku\algomidtermm\IO_A_1_10\1_in_10.txt

//import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author acer
 */
public class FindLongest {
//Given an array, A, of n integers, find the longest subarray of A such that 
//all the numbers in that subarray are in sorted order. 
//What is the running time of your method? 

    private static int[] FindLongestSubArray(int n, int[] A) {
        int count = 1;
        int max = 1;
        int start = 0;

        for (int i = 1; i < n; i++) {
            if (A[i] > A[i - 1]) { //check for sort order
                count++;
            } else {
                if (max < count) { //check for update max number of sort order integers
                    start = i - count;
                    max = count;
                }
                count = 1;
            }
        }
        if (max < count) { //compare max with count last subarray for recheck
            start = n - count;
            max = count;
        }
        int[] longestSub = new int[max];
        for (int i = 0; i < max; i++) {
            longestSub[i] = A[i + start];
        }
        //longestSub = Arrays.copyOfRange(A, start, max + start);
        return longestSub;
    }

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
//        System.out.println("Please Enter number of integers in array and Enter your array : ");
        int n = kb.nextInt();
        int[] A = new int[n];
        for (int i = 0; i < n; i++) {
            A[i] = kb.nextInt();
        }
        int[] longestSub = FindLongestSubArray(n, A);
        for (int i = 0; i < longestSub.length; i++) {
            System.out.print(longestSub[i] + " ");
        }

    }

}
