/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.mukku.algomidtermm;

// cmd; java com\mukku\algomidtermm\ConvertString.java < com\mukku\algomidtermm\IO_A_1_9\1_in_9.txt

import java.util.Scanner;

/**
 *
 * @author acer
 */
public class ConvertString {
//Given a string, S, of n digits in the range from 0 to 9, describe 
//an efficient algorithm for StringToInteger S into the integer it represents. 
//What is the running time of your algorithm? 

    private static int StringToInteger(String S) {

        int n = S.length(); //let n is length of S
        int sum = 0; //create sum for keep integer of String
//        sum = (S.charAt(0)- '0')*10; //get value integer of char and try to multiply with value each position
        for (int i = 0; i < n; i++) {
            sum += (S.charAt(i) - '0') * Math.pow(10, (n - 1 - i));
            //can use S.charAt(i) - '0' for get value integer of char
            //because in java if use char as int; it must return ASCII of each char
            //then can do this with "- '0'" that is get different between ASCII of them
            /*
            char               ASCII of char        different=integer value
            '0'                       48           '0'-'0'        48-48 = 0
            '1'                       49           '1'-'0'        49-48 = 1
            '2'                       50           '2'-'0'        50-48 = 2
            '3'                       51           '3'-'0'        51-48 = 3
            '4'                       52           '4'-'0'        52-48 = 4
            '5'                       53           '5'-'0'        53-48 = 5
            '6'                       54           '6'-'0'        54-48 = 6
            '7'                       55           '7'-'0'        55-48 = 7
            '8'                       56           '8'-'0'        56-48 = 8
            '9'                       57           '9'-'0'        57-48 = 9
            */
        }
        return sum;
//       System.out.println(sum); //show integer sum
//       System.out.println(((Object) sum).getClass().getSimpleName());  //view type of sum -- integer
    }

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
//        System.out.println("Please Enter String of n digits in the range from 0 to 9 : ");
        String S = kb.next(); //input string of integers
//        System.out.println(S); //show string in S
//        System.out.println(S.getClass().getSimpleName()); //view type of S -- string
        System.out.println(StringToInteger(S));
    }

}
